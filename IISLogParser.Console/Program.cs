﻿using System;
using IISLogParser.Core;

namespace IISLogParser
{
    class Program
    {

        #region "Static console arguments"

        private const string ARG_FILENAME = "FILENAME";
        private const string ARG_EXIT = "EXIT";
        private const string ARG_REPORT = "REPORT";
        private const string ARG_USER_AGENT = "USER-AGENT";
        private const string ARG_REQUEST_URL = "REQUEST-URL";

        private static ILogReporter reporter;

        #endregion

        static void Main(string[] args)
        {
            reporter = new LogReporter();

            Console.WriteLine("*** Welcome to the iis log file reporter. Make sure you set a log filename before you try to report ***");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("List of commands:");
            Console.WriteLine("filename=[absolute-file-name-here]");
            Console.WriteLine("user-agent=[myuseragentstringhere] <- will provide a count of unique requests with that user agent string. Is fuzzy, so 'pingdom' will still find pingdom requests.");
            Console.WriteLine("request-url=[myurltosearchfor] <- will provide a count of unique requests with that request url (excluding any resources on the page). Is fuzzy, so /contact may find more requests than you intend.");
            Console.WriteLine("report");
            

            // Capture user input forever, or until you close the app
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Please input instruction: ");
                var userInput = Console.ReadLine();

                if (string.IsNullOrEmpty(userInput))
                {
                    Console.WriteLine("No argument specified. Please specify an argument");
                    continue;
                }

                var arguments = userInput.Split(new char[] {'='});
                ProcessUserInput(arguments);
            }
        }

        private static void ProcessUserInput(string[] arguments)
        {
            switch (arguments[0].ToUpper())
            {
                case ARG_FILENAME:
                    reporter.FileName = arguments[1];
                    Console.WriteLine("Filename set. You can try reporting now");
                    break;
                case ARG_REQUEST_URL:
                    reporter.RequestUrlFilter = arguments[1];
                    Console.WriteLine("Request URL filter set.");
                    break;
                case ARG_USER_AGENT:
                    reporter.UserAgentFilter = arguments[1];
                    Console.WriteLine("User Agent filter set.");
                    break;
                case ARG_REPORT:
                    try
                    {
                        Console.WriteLine("Reporting...");
                        FireReport();
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("Reporting error: {0}", ex.Message);
                        Console.WriteLine("");
                    }
                    
                    break;
                case ARG_EXIT:
                    Environment.Exit(0);
                    break;
            }
        }

        private static void FireReport()
        {
            var report = reporter.GenerateReport();

            try
            {
                WriteOutput(report);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void WriteOutput(Report report)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("*** SUMMARY ***");
            Console.WriteLine("There were {0} lines.", report.LineCount); 
            Console.WriteLine("There were {0} unique IPs", report.UniqueRequestsCount);
            Console.WriteLine("{0} of total requests were bot requests", report.BotCount);
            Console.WriteLine("{0} of the bot requests were pingdoms", report.PingdomCount);
            Console.WriteLine("{0} requests were for a newsletter email campaign page", report.NewsletterRequestCount);
            Console.WriteLine("{0} requests were related to a newsletter email campaign page", report.NewsletterRelatedRequestsCount);
            
            if (!string.IsNullOrEmpty(reporter.UserAgentFilter))
            {
                Console.WriteLine("{0} requests came from your specified user agent '{1}'", report.FilteredUserAgentCount, reporter.UserAgentFilter);
            }
            if (!string.IsNullOrEmpty(reporter.RequestUrlFilter))
            {
                Console.WriteLine("{0} requests were for your specified request url '{1}'", report.FilteredRequestUrlCount, reporter.RequestUrlFilter);
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
