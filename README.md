# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is intended as a console app to take an iis log file, parse it, and give you some information that'll help you debug a site quickly.

* It'll eventually tell you how many unique  visitors there were
* How many were robots crawling it
* Version 0.1 - Not really usable yet
* Most popular url, and how many unique requests
* How many individual requests belonged to that url request

### How do I get set up? ###

* Run it, and specify an IIS log file
* You'll be able to specify arguments for what you want to know, eventually

### Contribution guidelines ###

* None. Nobody's going to care for a while. It should probably be written in something like Python anyway

### Who do I talk to? ###

* Me