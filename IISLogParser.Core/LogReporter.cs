﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace IISLogParser.Core
{
    public class LogReporter : ILogReporter
    {
        private const int SOURCE_IP_ADDRESS = 8;
        private const int URL_REQUESTED = 4;
        private const int USER_AGENT = 13;
        private const int ORIGINAL_REQUEST_URL = 16;

        private const string REGEXPATTERN = @"^([\d]*-[\d]*-[\d]* [\d]*:[\d]*:[\d]*) ([\d]*.[\d]*.[\d]*.[\d]*) (GET|SET|HEAD|POST) ([\/](.)*)( \- )?(443|80) - ((\d)*.(\d)*.(\d)*.(\d)*) ([\.a-zA-Z\/_\d\(\:\+\;\,\-\)]*) (https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*))?(\-)?( )?(\d* \d* \d* \d*)$";

        public string FileName { get; set; }
        public string UserAgentFilter { get; set; }
        public string RequestUrlFilter { get; set; }

        public Report GenerateReport()
        {
            if (string.IsNullOrEmpty(this.FileName))
            {
                throw new ArgumentException("No Filename specified");
            }

            var lineCounter = 0;
            var botCounter = 0;
            var pingdomCounter = 0;
            var newsletterRequestCounter = 0;
            var relatedToNewsletterCounter = 0;
            var filteredRequestUrlCounter = 0;
            var filteredUserAgentCounter = 0;

            string currentLineToRead;

            var uniqueRequests = new Dictionary<string, string>();

            var rx = new Regex(REGEXPATTERN,
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            StreamReader file;

            try
            {
                // Read the file and display it line by line.  
                file = new StreamReader(@FileName);
            }
            catch (FileNotFoundException)
            {
                throw new ArgumentException("Couldn't find that file. Try again.");
            }
            
            while ((currentLineToRead = file.ReadLine()) != null)
            {
                // Find matches.
                var matches = rx.Matches(currentLineToRead);

                // Report on each match.
                foreach (Match match in matches)
                {
                    var groups = match.Groups;

                    if (!uniqueRequests.ContainsKey(groups[SOURCE_IP_ADDRESS].ToString()))
                    {
                        uniqueRequests.Add(groups[SOURCE_IP_ADDRESS].ToString(), groups[URL_REQUESTED].ToString());
                    }

                    if (!string.IsNullOrEmpty(this.RequestUrlFilter))
                    {
                        if (groups[URL_REQUESTED].ToString().ToLower().Contains(this.RequestUrlFilter.ToLower()))
                        {
                            filteredRequestUrlCounter++;
                        }
                    }

                    if (!string.IsNullOrEmpty(this.UserAgentFilter))
                    {
                        if (groups[USER_AGENT].ToString().ToLower().Contains(this.UserAgentFilter.ToLower()))
                        {
                            filteredUserAgentCounter++;
                        }
                    }

                    if (groups[URL_REQUESTED].ToString().ToLower().Contains("utm_medium"))
                    {
                        newsletterRequestCounter++;
                    }

                    if (groups[ORIGINAL_REQUEST_URL].ToString().ToLower().Contains("utm_medium"))
                    {
                        relatedToNewsletterCounter++;
                    }

                    if (groups[USER_AGENT].ToString().Contains("bot"))
                    {
                        if (groups[USER_AGENT].ToString().Contains("pingdom"))
                        {
                            pingdomCounter++;
                        }

                        botCounter++;
                    }

                    lineCounter++;
                }
            }

            file.Close();  
            file.Dispose();

            var report = new Report()
            {
                BotCount = botCounter,
                FileName = this.FileName,
                LineCount = lineCounter,
                NewsletterRelatedRequestsCount = relatedToNewsletterCounter,
                NewsletterRequestCount = newsletterRequestCounter,
                PingdomCount = pingdomCounter,
                UniqueRequestsCount = uniqueRequests.Count,
                FilteredRequestUrlCount = filteredRequestUrlCounter,
                FilteredUserAgentCount = filteredUserAgentCounter
            };

            return report;
        }
    }

    public interface ILogReporter
    {
        string FileName { get; set; }
        /// <summary>
        /// We'll count the number of requests that have that user agent
        /// </summary>
        string UserAgentFilter { get; set; }

        /// <summary>
        /// We'll count the number of requests that have that request url (Won't include requests for any other resources on that page)
        /// </summary>
        string RequestUrlFilter { get; set; }

        Report GenerateReport();
    }
}
