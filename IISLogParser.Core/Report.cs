﻿namespace IISLogParser.Core
{
    public class Report
    {
        public string FileName { get; set; }
        public int LineCount { get; set; }
        public int BotCount { get; set; }
        public int PingdomCount { get; set; }
        public int NewsletterRequestCount { get; set; }
        public int NewsletterRelatedRequestsCount { get; set; }
        public int UniqueRequestsCount { get; set; }
        public int FilteredRequestUrlCount { get; set; }
        public int FilteredUserAgentCount { get; set; }
    }
}
